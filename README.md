# GitHub Pull Closer

GitHub Pull Closer is in charge of closing pull requests against [Arch's read-only mirrors on GitHub](https://github.com/archlinux/?q=read-only) and referring them to our GitLab instance.

It utilities [GitHub's webhook functionally](https://docs.github.com/en/developers/webhooks-and-events/webhooks/about-webhooks) [to trigger a GitLab pipeline](https://docs.gitlab.com/ee/ci/triggers/#triggering-a-pipeline-from-a-webhook) which runs a script that uses [GitHub's REST API](https://docs.github.com/en/rest) to close the PR.
